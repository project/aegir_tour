<?php
/**
 * @file
 * aegir_tour.bootstrap_tour_tour.inc
 */

/**
 * Implements hook_default_bootstrap_tour().
 */
function aegir_tour_default_bootstrap_tour() {
  $export = array();

  $bootstrap_tour = new stdClass();
  $bootstrap_tour->disabled = FALSE; /* Edit this to true to make a default bootstrap_tour disabled initially */
  $bootstrap_tour->api_version = 1;
  $bootstrap_tour->name = 'aegir_introduction';
  $bootstrap_tour->title = 'Aegir Introduction';
  $bootstrap_tour->roles = '';
  $bootstrap_tour->autorun = 1;
  $bootstrap_tour->description = 'An introductory tour of the Aegir Hosting System';
  $bootstrap_tour->steps = array(
    0 => array(
      'selector' => '',
      'path' => '<front>',
      'placement' => 'top',
      'title' => 'Welcome to the Aegir Hosting System',
      'content' => 'Hi there!

If this is your first time using Aegir, we recommend that you follow this brief tour, so we can explain some of the features and concepts behind your new hosting system.',
      'format' => 'plain_text',
    ),
    1 => array(
      'selector' => '.active-trail .active',
      'path' => 'hosting/sites',
      'placement' => 'left',
      'title' => 'Sites',
      'content' => 'Let\'s start by reviewing sites.

Sites are exactly what you\'d expect: fully installed Drupal sites, with their own database, setting.php file, etc.',
      'format' => 'plain_text',
    ),
    2 => array(
      'selector' => '.views-table .cols-5',
      'path' => '',
      'placement' => 'top',
      'title' => 'Site list',
      'content' => 'Here you have a list of all the sites installed on this Aegir system.

It can be sorted and filtered to make finding a particular site easier. You also have the ability to run various tasks (or operations), such as running a backup, on one or more sites. We\'ll explain tasks in more detail shortly.',
      'format' => 'plain_text',
    ),
    3 => array(
      'selector' => 'ul.tabs.primary li a',
      'path' => '',
      'placement' => 'right',
      'title' => 'Add a new site',
      'content' => 'Sites can be added by clicking the \'Add site\' link.

Go ahead and click that now, so we can see how easy it is to install a new Drupal site in Aegir.',
      'format' => 'plain_text',
    ),
  );
  $export['aegir_introduction'] = $bootstrap_tour;

  return $export;
}
