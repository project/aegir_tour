<?php
/**
 * @file
 * aegir_tour.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function aegir_tour_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "bootstrap_tour" && $api == "bootstrap_tour_tour") {
    return array("version" => "1");
  }
}
